global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define sys_read 0
%define sys_write 1
%define sys_exit 60
%define stdin 0
%define stdout 1

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, sys_exit
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax    
.loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov r11, rax
    
    mov rax, sys_write
    mov rdx, r11
    mov rsi, rdi
    mov rdi, stdout

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:    
    mov rax, sys_write
    push rdi
    mov rdi, stdout
    mov rsi, rsp
    mov rdx, 1 ; Number of characters to out

    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov r11, `\n` ; Символ переноса строки -> r11
    call print_char
    ret;


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10 ; Divider
    xor r9, r9
.loop:
    xor rdx, rdx
    div r10
    add rdx, '0'

    dec rsp
    mov byte[rsp], dl
    inc r9

    cmp rax, 0
    jnz .loop
.print:
    mov rax, sys_write
    mov rdi, stdout
    mov rdx, r9
    mov rsi, rsp
    syscall
    add rsp, r9
.end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
.negative:
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .zero1

    cmp byte[rsi+rax], 0
    je .zero2

    mov dl, [rdi+rax]
    mov cl, [rsi+rax]
    cmp dl, cl
    jne .false
    
    inc rax
    jmp .loop    

.zero1:
    cmp byte[rsi+rax], 0
    je .true
    jmp .false

.zero2:
    cmp byte[rdi+rax], 0
    je .true
    jmp .false    
    
.true:
    mov rax, 1
    ret

.false:
    mov rax, 0
    ret    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, sys_read
    dec rsp
    mov rsi, rsp
    mov rdx, 1 ; Number of characters to in 
    syscall

    cmp rax, 0
    je .eof
    mov al, byte[rsp]
    inc rsp
    ret 
.eof:
    inc rsp
    ret    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
.loop:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi

    cmp rax, 0    
    je .happyend    

    cmp rdx, 0
    jne .read_any
    
    cmp rax, `\t`
    je .loop
    cmp rax, `\n`
    je .loop
    cmp rax, ` `
    je .loop
.read_any:
    cmp rdx, rsi
    je .badend

    cmp rax, `\t`
    je .happyend
    cmp rax, `\n`
    je .happyend
    cmp rax, ` `
    je .happyend 

    mov byte[rdi+rdx], al    

    inc rdx
    jmp .loop

.happyend:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret

.badend:
    ;mov byte[rdi+rdx], 0
    mov rax, 0
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    mov r11, 10 ; Divider
.loop:
    mov cl, byte[rdi+rdx]
    cmp cl, 0    
    je .end

    cmp cl, '0'
    jl .end
    cmp cl, '9'
    jg .end
    
    sub cl, '0'

    push rdx
    mul r11
    pop rdx

    add al, cl    
    inc rdx
    jmp .loop

.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx

    cmp byte[rdi], '-'
    je .negative
    jmp parse_uint
.negative:
    inc rdi 
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx
    neg rax
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r11, r11
.loop:
    cmp byte[rdi+r11], 0
    je .happyend        

    cmp r11, rdx
    je .badend

    mov al, byte[rdi+r11]
    mov byte[rsi+r11], al
    inc r11
    jmp .loop

.badend:
    mov byte[rsi+r11], 0
    mov rax, 0
    ret

.happyend:
    mov byte[rsi+r11], 0
    mov rax, r11
    ret













