global _start
extern print_string
extern find_word
extern read_word
extern string_length
extern exit


section .data
not_found_err_msg: db "Элемент не найден", 0
read_err_msg: db "Ошибка при чтении строки", 0

%include "words.inc"

%define sys_write 1
%define stderr 2
%define str_size 255

section .text
_start:
    sub rsp, str_size

    mov rdi, rsp
    mov rsi, str_size
    call read_word; (buffer_addr, buffer_size) -> (str_addr, str_size)
    cmp rax, 0
    je .read_err
    
    mov rdi, rax
    mov rsi, next    
    call find_word
    cmp rax, 0
    je .not_found_err

    mov rdi, rax
    add rsp, str_size

    call print_string
    call exit

.not_found_err:
    mov rdi, not_found_err_msg
    jmp .err_print

.read_err:
    mov rdi, read_err_msg

.err_print:
    call string_length
    mov r11, rax
    mov rax, sys_write
    mov rdx, r11
    mov rsi, rdi
    mov rdi, stderr
    syscall
    call exit
