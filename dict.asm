extern string_equals
extern string_length
global find_word

;rdi - принимает указатель на нуль-терминированную строку
;rsi - принимает указатель на начало словаря
;; rax - возвращает адрес начала вхождения в словарь или 0, если вхождение не
;; найдено 
find_word:
.loop:
    cmp rsi, 0; указатель нулевой - конец словаря
    jz .not_found
      
    push rdi
    push rsi
    add rsi, 8; ставим указатель на ключ 
    call string_equals; возвращает 1, если строки равны, 0 иначе
    pop rsi
    pop rdi
    cmp rax, 1
    je .found

    mov rsi, [rsi]; ставим указатель на следующий блок
    jmp .loop

.found:
    call string_length
    add rax, rsi
    add rax, 9
    ret

.not_found:
    xor rax, rax
    ret    
    
