%define next 0

%macro colon 2
    %2:
        dq next
        %define next %2
        db %1, 0
%endmacro
